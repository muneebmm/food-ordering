import Image from 'next/image'
import {Button} from "antd"
import { Inter } from 'next/font/google'
import {Typography} from "antd"
import Header from '@/components/header'
import Footer from '@/components/footer'

const {Title} = Typography

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
    >
    <Header />
    <Footer />

      
    </main>
  )
}
